poolcounter-rs
==============

A re-implementation of <https://www.mediawiki.org/wiki/PoolCounter>, but in
Rust. Mostly for learning purposes, but also to see what can be improved.

It currently passes ~41% of the PoolCounter test suite.
