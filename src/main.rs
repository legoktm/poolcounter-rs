/**
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use anyhow::Result;
use std::io;
use std::sync::Arc;
use tokio::net::{TcpListener, TcpStream};

mod locks;
mod protocol;
mod waker;

use locks::{LockManager, LockTake};
use protocol::{Command, Error, Response};
use waker::Waker;

#[tokio::main]
async fn main() -> Result<()> {
    let listener = TcpListener::bind("127.0.0.1:7531").await?;
    let manager = Arc::new(LockManager::default());
    let waker = Arc::new(Waker::default());

    loop {
        let (socket, addr) = listener.accept().await?;
        let manager = manager.clone();
        let waker = waker.clone();
        tokio::spawn(async move {
            println!("new connection: {:?}", &addr);
            // FIXME: unwrap
            process(socket, manager, waker).await.unwrap();
            println!("closed connection: {:?}", &addr);
        });
    }
}

#[derive(Eq, PartialEq, Debug, Clone)]
enum Status {
    None,
    LockedForAny(String),
    LockedForMe(String),
}

async fn handle_msg(
    msg: Vec<u8>,
    status: &Status,
    lock_manager: &Arc<LockManager>,
    waker: &Arc<Waker>,
) -> (Response, Status) {
    let cmd = match Command::new(&msg) {
        Ok(cmd) => cmd,
        Err(err) => {
            return (Response::Error(err), status.clone());
        }
    };
    dbg!(&cmd);
    match cmd {
        Command::Acq4Any(key, active, total, timeout) => {
            if status != &Status::None {
                return (Response::LockHeld, status.clone());
            }
            let taken = {
                let mut any_locks = lock_manager.any_locks.write().await;
                let lock = any_locks.entry(key.to_string()).or_default();

                lock.take(active, total)
            };
            match taken {
                LockTake::LockedActive => {
                    (Response::Locked, Status::LockedForAny(key))
                }
                LockTake::LockedTotal => {
                    waker.wait_for_any(key.clone()).await;
                    // Release the total lock
                    let mut any_locks = lock_manager.any_locks.write().await;
                    let lock = any_locks.entry(key.to_string()).or_default();

                    lock.release_total();
                    (Response::Done, Status::None)
                }
                LockTake::Failed => (Response::QueueFull, Status::None),
            }
        }
        Command::Acq4Me(key, active, total, timeout) => {
            if status != &Status::None {
                return (Response::LockHeld, status.clone());
            }
            let taken = {
                let mut me_locks = lock_manager.me_locks.write().await;
                let lock = me_locks.entry(key.clone()).or_default();
                lock.take(active, total)
            };
            match taken {
                LockTake::LockedActive => {
                    (Response::Locked, Status::LockedForMe(key))
                }
                LockTake::LockedTotal => {
                    waker.wait_for_me(key.clone()).await;
                    (Response::Locked, Status::LockedForMe(key))
                }
                LockTake::Failed => (Response::QueueFull, Status::None),
            }
        }
        Command::Release => match &status {
            Status::LockedForAny(key) => {
                lock_manager.release_any(&key).await;
                waker.wake_any(key).await;
                (Response::Released, Status::None)
            }
            Status::LockedForMe(key) => {
                lock_manager.release_me(&key).await;
                waker.wake_me(key).await;
                (Response::Released, Status::None)
            }
            _ => (Response::NotLocked, Status::None),
        },
        _ => (Response::Error(Error::BadCommand), status.clone()),
    }
}

async fn process(
    socket: TcpStream,
    lock_manager: Arc<LockManager>,
    waker: Arc<Waker>,
) -> Result<()> {
    let mut status = Status::None;
    loop {
        // Wait for the socket to be readable
        socket.readable().await?;

        // TODO: Is 1024 bytes enough?
        let mut msg = vec![0; 1024];

        // Try to read data
        match socket.try_read(&mut msg) {
            Ok(n) => {
                // Truncate to n-1 to trim the trailing newline
                if n >= 1 {
                    msg.truncate(n - 1);
                } else if n == 0 {
                    // Empty line, treat as socket close
                    // Unlock anything that might be locked
                    match &status {
                        Status::LockedForAny(key) => {
                            lock_manager.release_any(&key).await;
                            waker.wake_any(key).await;
                        }
                        Status::LockedForMe(key) => {
                            lock_manager.release_me(&key).await;
                            waker.wake_me(key).await;
                        }
                        Status::None => {}
                    }
                    return Ok(());
                }

                let (resp, new_status) =
                    handle_msg(msg, &status, &lock_manager, &waker).await;
                status = new_status;
                dbg!(&resp, &status);

                socket.writable().await?;
                socket.try_write(&resp.to_bytes())?;
            }
            Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                continue;
            }
            Err(e) => {
                return Err(e.into());
            }
        };
    }
}
