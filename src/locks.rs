/**
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use std::collections::HashMap;
use tokio::sync::RwLock;

#[derive(Default)]
pub struct LockManager {
    pub any_locks: RwLock<HashMap<String, Lock>>,
    pub me_locks: RwLock<HashMap<String, Lock>>,
}

impl LockManager {
    pub async fn release_me(&self, key: &str) {
        if let Some(lock) = self.me_locks.write().await.get_mut(key) {
            lock.release_active();
        }
    }

    pub async fn release_any(&self, key: &str) {
        if let Some(lock) = self.any_locks.write().await.get_mut(key) {
            lock.release_active();
        }
    }
}

#[derive(Default)]
pub struct Lock {
    active: usize,
    total: usize,
}

impl Lock {
    /// Try to take a lock
    pub fn take(&mut self, active: usize, total: usize) -> LockTake {
        if self.active < active {
            self.active += 1;
            self.total += 1;
            LockTake::LockedActive
        } else if self.total < total {
            self.total += 1;
            LockTake::LockedTotal
        } else {
            LockTake::Failed
        }
    }

    /// Release an active lock
    pub fn release_active(&mut self) {
        // TODO: check overflow
        self.active -= 1;
        self.total -= 1;
    }

    pub fn release_total(&mut self) {
        // TODO: check overflow
        self.total -= 1;
    }
}

pub enum LockTake {
    /// Took an active lock
    LockedActive,
    /// Took a total lock
    LockedTotal,
    /// Failed to get a lock, queue full
    Failed,
}
