/**
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

// Some UTF-8 byte constants
const SPACE: u8 = 32;

fn byte_to_usize(bytes: &[u8]) -> Result<usize, Error> {
    // TODO: optimize
    let string =
        String::from_utf8(bytes.to_vec()).map_err(|_| Error::Unknown)?;
    // Not an integer
    Ok(string.parse().map_err(|_| Error::BadSyntax)?)
}

fn no_zero(val: usize) -> Result<usize, Error> {
    if val == 0 {
        Err(Error::BadSyntax)
    } else {
        Ok(val)
    }
}

#[derive(Debug)]
pub enum StatsMode {
    Full,
    Uptime,
}

#[derive(Debug)]
pub enum Command {
    Acq4Any(String, usize, usize, usize),
    Acq4Me(String, usize, usize, usize),
    Release,
    Stats(StatsMode),
}

impl Command {
    pub fn new(raw: &[u8]) -> Result<Command, Error> {
        let sp: Vec<_> = raw.split(|byte| byte == &SPACE).collect();
        // TODO: bounds check?
        // dbg!(String::from_utf8(sp[0].to_vec()));
        match sp[0] {
            b"ACQ4ANY" => {
                if sp.len() != 5 {
                    return Err(Error::BadSyntax);
                }
                Ok(Command::Acq4Any(
                    String::from_utf8(sp[1].to_vec())
                        .map_err(|_| Error::Unknown)?,
                    no_zero(byte_to_usize(sp[2])?)?,
                    no_zero(byte_to_usize(sp[3])?)?,
                    byte_to_usize(sp[4])?,
                ))
            }
            b"ACQ4ME" => {
                if sp.len() != 5 {
                    return Err(Error::BadSyntax);
                }
                Ok(Command::Acq4Me(
                    String::from_utf8(sp[1].to_vec())
                        .map_err(|_| Error::Unknown)?,
                    no_zero(byte_to_usize(sp[2])?)?,
                    no_zero(byte_to_usize(sp[3])?)?,
                    byte_to_usize(sp[4])?,
                ))
            }
            b"RELEASE" => Ok(Command::Release),
            b"STATS" => {
                // TODO: bounds check
                dbg!(String::from_utf8(sp[1].to_vec()));
                match sp[1] {
                    b"FULL" => Ok(Command::Stats(StatsMode::Full)),
                    b"UPTIME" => Ok(Command::Stats(StatsMode::Uptime)),
                    _ => Err(Error::Unknown),
                }
            }
            _ => Err(Error::BadCommand),
        }
    }
}

#[derive(Debug)]
pub enum Error {
    BadCommand,
    BadSyntax,
    Unknown,
}

impl Error {
    pub fn to_bytes(&self) -> &[u8] {
        match self {
            Error::BadCommand => b"BAD_COMMAND",
            Error::BadSyntax => b"BAD_SYNTAX",
            Error::Unknown => b"UNKNOWN",
        }
    }
}

#[derive(Debug)]
pub enum Response {
    Locked,
    Done,
    QueueFull,
    Timeout,
    LockHeld,
    NotLocked,
    Released,
    Error(Error),
}

impl Response {
    pub fn to_bytes(&self) -> Vec<u8> {
        match self {
            Response::Locked => b"LOCKED\n".to_vec(),
            Response::Done => b"DONE\n".to_vec(),
            Response::QueueFull => b"QUEUE_FULL\n".to_vec(),
            Response::Timeout => b"TIMEOUT\n".to_vec(),
            Response::LockHeld => b"LOCK_HELD\n".to_vec(),
            Response::NotLocked => b"NOT_LOCKED\n".to_vec(),
            Response::Released => b"RELEASED\n".to_vec(),
            Response::Error(reason) => {
                // TODO: add in reason
                [b"ERROR ", reason.to_bytes(), b"\n"].concat()
            }
        }
    }
}
