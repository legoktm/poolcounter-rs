/**
Copyright (C) 2021 Kunal Mehta <legoktm@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::collections::HashMap;
use std::sync::Arc;
use tokio::sync::{Notify, RwLock};

#[derive(Default)]
pub struct Waker {
    me_handlers: RwLock<HashMap<String, Vec<Arc<Notify>>>>,
    any_handlers: RwLock<HashMap<String, Arc<Notify>>>,
}

impl Waker {
    /// Wake all threads waiting on a ANY lock
    pub async fn wake_any(&self, key: &str) {
        let notifier = {
            let mut handlers = self.any_handlers.write().await;
            handlers.remove(key)
        };
        if let Some(notifier) = notifier {
            // Notify all threads waiting
            notifier.notify_waiters()
        }
    }

    pub async fn wait_for_any(&self, key: String) {
        // First, get a notifier, creating one if necessary
        let notifier = {
            let mut handlers = self.any_handlers.write().await;
            handlers.entry(key).or_default().clone()
        };

        // Now wait to be notified
        notifier.notified().await
    }

    /// Wake a single thread that's waiting on a ME lock.
    pub async fn wake_me(&self, key: &str) {
        let notifier = {
            let mut handlers = self.me_handlers.write().await;
            let mut list = handlers.get_mut(key);
            match list {
                Some(notifiers) => notifiers.pop(),
                None => None,
            }
        };
        if let Some(notifier) = notifier {
            notifier.notify_one();
        }
    }

    pub async fn wait_for_me(&self, key: String) {
        // First, register a notifier
        let notify = Arc::new(Notify::new());
        {
            let mut handlers = self.me_handlers.write().await;
            let mut list = handlers.entry(key).or_default();
            list.push(notify.clone());
        }

        // Now wait to be notified
        notify.notified().await
    }
}
